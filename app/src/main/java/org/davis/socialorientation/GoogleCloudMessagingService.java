package org.davis.socialorientation;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Created by davis on 31/05/16.
 */

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class GoogleCloudMessagingService extends IntentService {

    public static final String TAG = "GCMService";

    private static final int NOTIFICATION_ID = 7171;

    public static final String ACTION_REPLY_TO_MESSAGE = "org.davis.socialorientation.reply_to_msg";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public GoogleCloudMessagingService(String name) {
        super(name);
    }

    public GoogleCloudMessagingService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty() &&
                GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
            sendNotification(extras.getString("sender"), extras.getString("message"),
                    extras.getString("senderId"));
        }
        GoogleCloudMessagingReceiver.completeWakefulIntent(intent);
    }

    private void sendNotification(String sender, String msg, String senderId) {
        NotificationManager mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                resultIntent, 0);

        Intent replyIntent = new Intent(this, MainActivity.class);
        replyIntent.setAction(ACTION_REPLY_TO_MESSAGE);
        replyIntent.putExtra("replyTo", senderId);
        PendingIntent pReply = PendingIntent.getActivity(this, 1, replyIntent,
                0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(sender)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .addAction (R.drawable.ic_action_content_send, "Reply", pReply)
                        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
