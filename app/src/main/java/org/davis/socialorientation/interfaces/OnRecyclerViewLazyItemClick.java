package org.davis.socialorientation.interfaces;

/*
 * Created by Dāvis Mālnieks on 01/07/2015
 */

import android.view.View;

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public interface OnRecyclerViewLazyItemClick {
    void onIndividualItemClick(View v);

    void onItemClick(View v);

    void onItemLongClick(View v);
}