package org.davis.socialorientation.fragments.dialogs;

/*
 * Created by davis on 16/05/16.
 */

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.davis.socialorientation.R;
import org.davis.socialorientation.containers.Person;
import org.davis.socialorientation.database.preferences.SharedPreferencesHelper;

import java.text.DecimalFormat;

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class StatusDialogFragment extends DialogFragment {

    public static final String TAG = "df_status";

    // UI

    EditText mEditText;

    // Data

    public static final int TYPE_SET_MESSAGE = 0;

    public static final int TYPE_VIEW_PERSON = 1;

    int mDialogType = TYPE_SET_MESSAGE;

    public static StatusDialogFragment newInstance(Person person) {
        Bundle args = new Bundle();

        args.putString("name", person.getName());
        args.putString("message", person.getMessage());
        args.putDouble("distance", person.getDistance());
        args.putInt("type", TYPE_VIEW_PERSON);

        StatusDialogFragment fragment = new StatusDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(getArguments() != null) {
            mDialogType = getArguments().getInt("type", TYPE_SET_MESSAGE);
        }

        // Inflate layout
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Create dialog
        AlertDialog.Builder statusAlert = new AlertDialog.Builder(getActivity());
        View view;
        TextView headerText, infoText;
        switch (mDialogType) {
            case TYPE_SET_MESSAGE:
                view = inflater.inflate(R.layout.dialog_fragment_status, null);
                mEditText = (EditText) view.findViewById(R.id.edit_text_status);

                // Header
                headerText = (TextView) view.findViewById(R.id.info_text_status_heading);
                if(getStatusMessage() != null) {
                    headerText.setText(Html.fromHtml("<b>Your current status message:</b>"));
                } else {
                    headerText.setText(Html.fromHtml(headerText.getText().toString()));
                }

                // Message
                infoText = (TextView) view.findViewById(R.id.info_text_status);
                if(getStatusMessage() != null) {
                    infoText.setText(getStatusMessage());
                }

                statusAlert.setTitle("Status message");
                statusAlert.setView(view)
                        .setPositiveButton("Set", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                boolean saved = false;
                                if (mEditText != null) {
                                    Log.d(TAG, "Value is: " + mEditText.getText());

                                    saved = saveStatusMessage(mEditText.getText().toString());
                                }

                                String toastMessage = saved ? "Done!" : "Could not set your message!";
                                Toast.makeText(getActivity(), toastMessage, Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dismiss();
                            }
                        });
                break;
            case TYPE_VIEW_PERSON:
                view = inflater.inflate(R.layout.dialog_fragment_status_view, null);

                // Header
                headerText = (TextView) view.findViewById(R.id.info_text_status_heading);

                String message = getArguments().getString("message");
                if(message != null && message.length() != 0) {
                    headerText.setText(Html.fromHtml("<b>Few words about this person:</b>"));
                } else {
                    headerText.setText(Html.fromHtml(headerText.getText().toString()));
                }

                // Info
                infoText = (TextView) view.findViewById(R.id.info_text_status);
                infoText.setText(message);

                // Distance
                TextView distance = (TextView) view.findViewById(R.id.info_text_status_distance);
                distance.setText(Html.fromHtml("This person is approximately <b>" +
                        (new DecimalFormat("##.##").format(getArguments().getDouble("distance") * 1000))
                        + " meters</b> away from you!"));

                // Dialog
                statusAlert.setTitle(getArguments().getString("name"));
                statusAlert.setView(view)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dismiss();
                            }
                        });
                break;
        }

        return statusAlert.create();
    }

    // Gets users' status message
    private String getStatusMessage() {
        return SharedPreferencesHelper.getStringPreference(getActivity(),
                SharedPreferencesHelper.Keys.USER_STATUS, null);
    }

    // Saves users' status message
    private boolean saveStatusMessage(String message) {
        if(message == null || message.length() == 0) {
            return false;
        } else {
            SharedPreferencesHelper.saveStringPreference(getActivity(),
                    SharedPreferencesHelper.Keys.USER_STATUS, message);
        }

        return true;
    }
}
