package org.davis.socialorientation.fragments.containers;

import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.davis.socialorientation.MainActivity;
import org.davis.socialorientation.R;
import org.davis.socialorientation.config.Config;
import org.davis.socialorientation.containers.Person;
import org.davis.socialorientation.database.preferences.SharedPreferencesHelper;
import org.davis.socialorientation.fragments.FragmentMap;
import org.davis.socialorientation.fragments.FragmentPeople;
import org.davis.socialorientation.fragments.FragmentSettings;
import org.davis.socialorientation.helpers.UIHelper;
import org.davis.socialorientation.loaders.UserDataPoster;
import org.davis.socialorientation.tasks.SimpleAsync;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by davis on 06/04/16.
 */

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class FragmentHome extends Fragment implements MainActivity.ActivityFragmentBridge,
        View.OnClickListener, SlidingUpPanelLayout.PanelSlideListener {

    public static final String TAG = "f_home";

    // UI

    Toolbar mToolbar;

    SlidingUpPanelLayout mSlideUpMap;

    // Google

    public GoogleSignInAccount mGoogleSignInAccount;

    Location mLocation;

    // Data

    static Handler mHandler;

    static boolean mHandlerRunning = false;

    List<OnRefreshData> mRefrehRequestListener = new ArrayList<>();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Get accent color
        int accentColor = UIHelper.getAccentColor(getActivity());

        // Toolbar
        mToolbar = (Toolbar) getActivity().findViewById(R.id.main_toolbar);
        mToolbar.setTitle(getResources().getString(R.string.app_name));
        mToolbar.setBackgroundColor(accentColor);
        mToolbar.setTitleTextColor(Color.WHITE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mToolbar.setElevation(5);
        }

        ((MainActivity) getActivity()).setToolbar(mToolbar, false);

        FragmentMap fragmentMap = new FragmentMap();
        FragmentPeople fragmentPeople = new FragmentPeople();

        // Add and show the fragment
        getChildFragmentManager().beginTransaction()
                .add(R.id.slide_up_child_fragment, fragmentMap, FragmentMap.TAG)
                .add(R.id.activity_main_fragment_container, fragmentPeople, FragmentPeople.TAG)
                .commit();

        // Add on back button listener
        ((MainActivity) getActivity()).addOnBackPressedListener(this);

        // Slide up map
        mSlideUpMap = (SlidingUpPanelLayout) getActivity().findViewById(R.id.slide_up_map);
        mSlideUpMap.addPanelSlideListener(this);

        // FAB
        FloatingActionButton floatingActionButton =
                (FloatingActionButton) getActivity().findViewById(R.id.map_refresh);
        floatingActionButton.setOnClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onBackPressed() {
        if (mSlideUpMap != null && (mSlideUpMap.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED
                || mSlideUpMap.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED)) {

            mSlideUpMap.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

            return true;
        }
        return false;
    }

    @Override
    public void onLoggedIn(boolean success, GoogleSignInAccount signInAccount) {
        if(!success) Log.d(TAG, "Did not log in");

        FragmentMap fragmentMap =
                (FragmentMap) getChildFragmentManager().findFragmentByTag(FragmentMap.TAG);

        mGoogleSignInAccount = signInAccount;

        if(fragmentMap != null && mLocation != null) {
            fragmentMap.initialize(signInAccount, mLocation);

            //mGoogleSignInAccount = null;
            //mLocation = null;
            startHandler();
        }
    }

    @Override
    public void onLocationConnected(Location location) {
        FragmentMap fragmentMap =
                (FragmentMap) getChildFragmentManager().findFragmentByTag(FragmentMap.TAG);

        mLocation = location;

        if(fragmentMap != null && mGoogleSignInAccount != null) {
            fragmentMap.initialize(mGoogleSignInAccount, location);

            //mGoogleSignInAccount = null;
            //mLocation = null;
            startHandler();
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.map_refresh) {
            Log.d(TAG, "Clicked refresh button!");
            // Update user data on the cloud
            new SimpleAsync<Void, String, Void>(getActivity()) {

                @Override
                protected String process(Void input) {
                    Log.d(TAG, "About to post user data!");

                    if(mLocation == null || mGoogleSignInAccount == null) {
                        Log.d(TAG, "No google sign in or location, stop");
                        return "error:";
                    }

                    // Data
                    Person localUser = new Person(mGoogleSignInAccount.getDisplayName(), mGoogleSignInAccount.getId(),
                            mLocation.getLongitude(), mLocation.getLatitude(),
                            -1, // TODO
                            SharedPreferencesHelper.getStringPreference(mContext,
                                    SharedPreferencesHelper.Keys.USER_STATUS, null), true);

                    List<Person> data = new ArrayList<>();
                    data.add(localUser);
                    UserDataPoster userDataPoster = new UserDataPoster(mContext);
                    return userDataPoster.post(Config.Api.POST, data);
                }

                @Override
                protected void onLoadFinished(String output) {
                    if(output != null && (output.contains("error:") || output.contains("exception:"))) {
                        Log.d(TAG, "Error: " + output);
                        Toast.makeText(mContext, "Could not update your data. General error.",
                                Toast.LENGTH_SHORT).show();
                        return;
                    } else if(output != null && output.equals("failed")) {
                        Toast.makeText(mContext, "Could not update your data. Server error.",
                                Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        Log.d(TAG, "Success: " + output);
                    }

                    // Update data
                    for(OnRefreshData onRefreshData : mRefrehRequestListener) {
                        if(onRefreshData != null) {
                            onRefreshData.onRefreshRequested(mGoogleSignInAccount, mLocation);
                        }
                    }
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Override
    public void onPanelSlide(View panel, float slideOffset) {

    }

    @Override
    public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState,
                                    SlidingUpPanelLayout.PanelState newState) {

    }

    private void startHandler() {
        if (mHandlerRunning) return;

        int period = SharedPreferencesHelper.getIntPreference(getActivity(),
                FragmentSettings.PREF_LOCATION_SYNC_TYPE, FragmentSettings.PREF_LOCATION_SYNC_DEFAULT);

        switch (period) {
            case 1:
                period = 15000;
                break;
            case 2:
                period = 30000;
                break;
            case 3:
                period = 60000;
        }

        mHandler = new Handler();
        mHandler.postDelayed(mRefresher, period);
    }

    private Runnable mRefresher = new Runnable() {
        @Override
        public void run() {
            mHandlerRunning = true;

            boolean update = SharedPreferencesHelper.getBooleanPreference(getActivity(),
                    FragmentSettings.PREF_LOCATION_SYNC, true);

            if(!update) {
                mHandlerRunning = false;
                return;
            }
            //Log.d(TAG, "Sync!");

            for(OnRefreshData onRefreshData : mRefrehRequestListener) {
                if(onRefreshData != null) {
                    onRefreshData.onRefreshRequested(mGoogleSignInAccount, mLocation);
                }
            }

            mHandlerRunning = false;
        }
    };

    public void addOnRefreshRequestListener(OnRefreshData onRefreshData) {
        mRefrehRequestListener.add(onRefreshData);
    }

    public interface OnRefreshData {
        void onRefreshRequested(GoogleSignInAccount signInAccount,
                                Location location);
    }
}
