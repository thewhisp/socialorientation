package org.davis.socialorientation.fragments.dialogs;

/*
 * Created by davis on 02/06/16.
 */

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import org.davis.socialorientation.MainActivity;
import org.davis.socialorientation.R;

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class MessageDialogFragment extends DialogFragment {

    public static final String TAG = "df_message";

    // UI

    EditText mEditText;

    public static MessageDialogFragment newInstance(String sender, String senderId,
                                                    String receiver, String receiverId) {
        Bundle args = new Bundle();

        args.putString("senderId", senderId);
        args.putString("sender", sender);
        args.putString("receiver", receiver);
        args.putString("receiverId", receiverId);

        MessageDialogFragment fragment = new MessageDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Inflate layout
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Create dialog
        AlertDialog.Builder statusAlert = new AlertDialog.Builder(getActivity());
        View view  = inflater.inflate(R.layout.dialog_fragment_send_message, null);
        mEditText = (EditText) view.findViewById(R.id.edit_text_message);

        statusAlert.setTitle("Send message to " + getArguments().getString("receiver"));
        statusAlert.setView(view)
                .setPositiveButton("Send", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (mEditText != null) {
                            //Log.d(TAG, "Value is: " + mEditText.getText());

                            ((MainActivity) getActivity()).sendNotification(getArguments().getString("sender"),
                                    getArguments().getString("senderId"),
                                    getArguments().getString("receiverId"),
                                    mEditText.getText().toString());
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dismiss();
                    }
                });
        return statusAlert.create();
    }
}
