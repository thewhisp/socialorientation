package org.davis.socialorientation.fragments;

import android.content.DialogInterface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.davis.socialorientation.MainActivity;
import org.davis.socialorientation.R;
import org.davis.socialorientation.config.Config;
import org.davis.socialorientation.containers.Person;
import org.davis.socialorientation.database.preferences.SharedPreferencesHelper;
import org.davis.socialorientation.fragments.containers.FragmentHome;
import org.davis.socialorientation.fragments.dialogs.MessageDialogFragment;
import org.davis.socialorientation.fragments.dialogs.StatusDialogFragment;
import org.davis.socialorientation.loaders.LoaderException;
import org.davis.socialorientation.loaders.NearbyUserGetter;
import org.davis.socialorientation.tasks.SimpleAsync;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by davis on 06/04/16.
 */

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class FragmentMap extends Fragment implements OnMapReadyCallback,
        FragmentHome.OnRefreshData {

    public static final String TAG = "f_map";

    private GoogleMap mMap;

    private List<Person> mPersons;

    List<Marker> mMarkerList;

    static boolean mZoomedIn;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FragmentHome fragmentHome = (FragmentHome)
                getActivity().getSupportFragmentManager().findFragmentByTag(FragmentHome.TAG);
        if(fragmentHome != null) {
            fragmentHome.addOnRefreshRequestListener(this);
        }
    }

    public void initialize(final GoogleSignInAccount signInAccount,
                           final Location location) {
        if(signInAccount == null) {
            Log.d(TAG, "No signInAccount");
            return; // todo
        }

        if(location == null) {
            Log.d(TAG, "No location");
            return;
        }

        new SimpleAsync<Void, List<Person>, Void>(getActivity()) {
            @Override
            protected List<Person> process(Void input) {
                // Create local user
                Person localPerson = new Person(signInAccount.getDisplayName(),
                        signInAccount.getId(), location.getLongitude(),
                        location.getLatitude(), -1,
                        SharedPreferencesHelper.getStringPreference(mContext,
                                SharedPreferencesHelper.Keys.USER_STATUS_MESSAGE, "No message :("), true);

                NearbyUserGetter nearbyUserGetter = new NearbyUserGetter(mContext);
                List<Person> persons = new ArrayList<>();

                // Add local person
                persons.add(localPerson);
                try {
                    List<Person> apiPeople = nearbyUserGetter.load(Config.Api.GET,
                            (Config.Api.GET_NEARBY + signInAccount.getId()));

                    if(apiPeople != null) {
                        persons.addAll(apiPeople);
                    }
                } catch (JSONException | LoaderException e) {
                    e.printStackTrace();
                }

                return persons;
            }

            @Override
            protected void onLoadFinished(List<Person> output) {
                if(getActivity() == null) return;

                mPersons = new ArrayList<>(output);

                // Init map
                setUpMap();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    // Sets up the map
    public void setUpMap() {
        if (mMap == null) {
            // Get the map asynchronously
            ((SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.location_map)).getMapAsync(this);

        } else {
            showPeopleOnMap();
        }
    }

    // Show a person on map
    private Marker showPersonOnMap(Person person) {
        // Check if we have location permission
        if(!MainActivity.hasLocationPermission(getActivity())) {
            Log.d(TAG, "No location permission");
            return null;
        }

        //mMap.setMyLocationEnabled(true);

        if(mMap == null) {
            Log.d(TAG, "Map is null");
            return null;
        }

        // Drop the marker on the map
        String title = person.getIsLocalUser() ? (person.getName() + " (you)") : person.getName();
        Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(person.getLatitude(),
                person.getLongitude())).title(title));

        // Zoom to the local user marker
        if(person.getIsLocalUser() && !mZoomedIn) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(person.getLatitude(),
                    person.getLongitude()), 12.0f));

            marker.showInfoWindow();
            mZoomedIn = true;
        }

        // Click listener
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                // Hacky
                int listPos = -1;
                for(int i = 0; i < mMarkerList.size(); i++) {
                    if(mMarkerList.get(i).getId().equals(marker.getId())) {
                        listPos = i;
                        break;
                    }
                }

                if(listPos < 1) {
                    return;
                }

                final int pos = listPos;
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(mPersons.get(listPos).getName())
                        .setPositiveButton("About", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                StatusDialogFragment statusDialogFragment =
                                        StatusDialogFragment.newInstance(mPersons.get(pos));
                                statusDialogFragment.show(getChildFragmentManager(),
                                        StatusDialogFragment.TAG + "_view");
                            }
                        })
                        .setNegativeButton("Send message", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                MessageDialogFragment messageDialogFragment = MessageDialogFragment.newInstance(mPersons.get(0).getName(),
                                        mPersons.get(0).getId(), mPersons.get(pos).getName(), mPersons.get(pos).getId());
                                messageDialogFragment.show(getActivity().getSupportFragmentManager(), MessageDialogFragment.TAG);
                            }
                        });

                builder.show();
            }
        });
        return marker;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Failed to get map
        if(googleMap == null) return;

        // Set the map
        mMap = googleMap;

        showPeopleOnMap();
    }

    // Shows people on map
    private void showPeopleOnMap() {
        // Clear map
        mMap.clear();

        mMarkerList = new ArrayList<>();
        // Show users on map
        if(mPersons != null) {
            for(Person p : mPersons) {
                mMarkerList.add(showPersonOnMap(p));
            }
        } else {
            Log.d(TAG, "No persons to show on map");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onRefreshRequested(GoogleSignInAccount signInAccount, Location location) {
        //Log.d(TAG, "Sync");
        initialize(signInAccount, location);
    }
}
