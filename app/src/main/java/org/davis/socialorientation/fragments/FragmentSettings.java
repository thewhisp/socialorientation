package org.davis.socialorientation.fragments;

/*
 * Created by Dāvis Mālnieks on 07/07/2015
 */

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.SwitchPreference;
import android.widget.Toast;

import org.davis.socialorientation.R;
import org.davis.socialorientation.config.Config;
import org.davis.socialorientation.database.preferences.SharedPreferencesHelper;
import org.davis.socialorientation.preference.PreferenceFragment;

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class FragmentSettings extends PreferenceFragment
        implements Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener {

    public static final String TAG = "f_settings";

    ListPreference mRadius, mSyncInterval;

    SwitchPreference mSync;

    Preference mGit;

    // Data

    public static final String PREF_LOCATION_RADIUS = "pref_location_zone";

    public static final String PREF_LOCATION_SYNC = "pref_auto_update";

    public static final String PREF_LOCATION_SYNC_TYPE = "pref_auto_update_interval";

    public static final String PREF_LINK_DEVELOPER_GIT = "pref_developer_git";

    // Default

    public static final int PREF_LOCATION_SYNC_DEFAULT = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);

        // Find preferences
        // Lists
        mRadius = (ListPreference) findPreference(PREF_LOCATION_RADIUS);
        mSyncInterval = (ListPreference) findPreference(PREF_LOCATION_SYNC_TYPE);

        // Switch
        mSync = (SwitchPreference) findPreference(PREF_LOCATION_SYNC);

        // Preference
        mGit = findPreference(PREF_LINK_DEVELOPER_GIT);
        mGit.setOnPreferenceClickListener(this);

        // Set initial values
        mRadius.setValueIndex(SharedPreferencesHelper.getIntPreference(getActivity(),
                PREF_LOCATION_RADIUS, PREF_LOCATION_SYNC_DEFAULT));
        mSyncInterval.setValueIndex(SharedPreferencesHelper.getIntPreference(getActivity(),
                PREF_LOCATION_SYNC_TYPE, 1));
        mSync.setEnabled(SharedPreferencesHelper.getBooleanPreference(getActivity(),
                PREF_LOCATION_SYNC, true));

        // Interval summary
        // Update the description
        mRadius.setSummary("Within " +
                getZoneRadius(getActivity()) + " meter radius");
        mSyncInterval.setSummary(getSummaryForSyncInterval());

        // Add listeners
        mRadius.setOnPreferenceChangeListener(this);
        mSyncInterval.setOnPreferenceChangeListener(this);
        mSync.setOnPreferenceChangeListener(this);
    }

    private String getSummaryForSyncInterval() {
        switch (SharedPreferencesHelper.getIntPreference(getActivity(),
                PREF_LOCATION_SYNC_TYPE, 1)) {
            case 0:
                return "Sync will occur every 15 seconds";
            case 1:
                return "Sync will occur every 30 seconds";
            case 2:
                return "Sync will occur every 1 minute";
        }

        return null;
    }

    // Reads the update interval as hours
    public static int getZoneRadius(final Context context) {
        int idx = SharedPreferencesHelper.getIntPreference(context, PREF_LOCATION_RADIUS, 0);

        switch (idx) {
            case 1:
                return 300;
            case 2:
                return 500;
            case 3:
                return 1000;
            case 4:
                return 2000;
            case 5:
                return 5000;
            case 6:
                return 10000;
            case 7:
                return 25000;
            case 8:
                return 50000;
            default:
                return 100;
        }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object o) {
        int newValue = 0;
        try {
            newValue = Integer.parseInt(o.toString());
        } catch (Exception e) {
            //
        }

        switch (preference.getKey()) {
            case PREF_LOCATION_RADIUS:
                SharedPreferencesHelper.saveIntPreference(getActivity(), PREF_LOCATION_RADIUS,
                        newValue);

                // Update the description
                mRadius.setSummary("Within " +
                        getZoneRadius(getActivity()) + " meter radius");
                break;
            case PREF_LOCATION_SYNC_TYPE:
                SharedPreferencesHelper.saveIntPreference(getActivity(), PREF_LOCATION_SYNC_TYPE,
                        newValue);

                // Update the description
                mSyncInterval.setSummary(getSummaryForSyncInterval());
                break;
            case PREF_LOCATION_SYNC:
                SharedPreferencesHelper.saveBooleanPreference(getActivity(), PREF_LOCATION_SYNC,
                        o.toString().equals("true"));

        }

        return true;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        if(preference.getKey().equals(PREF_LINK_DEVELOPER_GIT)) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(Config.DEVELOPER_LINK));
            startActivity(i);
        }
        return false;
    }
}
