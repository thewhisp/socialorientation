package org.davis.socialorientation.fragments;

import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import org.davis.socialorientation.GoogleCloudMessagingService;
import org.davis.socialorientation.MainActivity;
import org.davis.socialorientation.R;
import org.davis.socialorientation.adapters.lists.PersonAdapter;
import org.davis.socialorientation.config.Config;
import org.davis.socialorientation.containers.Person;
import org.davis.socialorientation.fragments.containers.FragmentHome;
import org.davis.socialorientation.fragments.dialogs.MessageDialogFragment;
import org.davis.socialorientation.fragments.dialogs.StatusDialogFragment;
import org.davis.socialorientation.interfaces.OnRecyclerViewLazyItemClick;
import org.davis.socialorientation.loaders.LoaderException;
import org.davis.socialorientation.loaders.NearbyUserGetter;
import org.davis.socialorientation.tasks.SimpleAsync;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by davis on 06/04/16.
 */

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class FragmentPeople extends Fragment implements
        SimpleAsync.ContentLoaderTaskListener<List<Person>>, MainActivity.ActivityFragmentBridge,
        OnRecyclerViewLazyItemClick, PopupMenu.OnDismissListener, PopupMenu.OnMenuItemClickListener,
        FragmentHome.OnRefreshData{

    public static final String TAG = "f_people";

    // DATA

    PersonAdapter mPersonAdapter;

    List<Person> mPersons;

    private int mPopupClickedItem = -1;

    GoogleSignInAccount mSignInAccount;

    // UI

    RecyclerView mRecyclerView;

    ProgressBar mProgressBar;

    static boolean handledMessageAction;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // TODO: Add and start showing a progress bar here
        //requestDataForAdapter();

        ((MainActivity) getActivity()).addOnBackPressedListener(this);

        FragmentHome fragmentHome = (FragmentHome)
                getActivity().getSupportFragmentManager().findFragmentByTag(FragmentHome.TAG);
        if(fragmentHome != null) {
            fragmentHome.addOnRefreshRequestListener(this);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_people, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Progress bar
        mProgressBar = (ProgressBar) getActivity().findViewById(R.id.person_load_bar);

        // Init actual list view
        mRecyclerView = (RecyclerView) getActivity().findViewById(R.id.person_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // Setup adapter
        mPersonAdapter = new PersonAdapter(getActivity()); // Empty adapter, load data asynchronously
        mPersonAdapter.setOnRecyclerViewLazyItemClick(this);

        // Attach adapter to list view
        mRecyclerView.setAdapter(mPersonAdapter);

    }

    // Show something when "About" is selected on overflow menu
    private void handleInfoAction(int pos) {
        StatusDialogFragment statusDialogFragment =
                StatusDialogFragment.newInstance(mPersonAdapter.getItem(pos));
        statusDialogFragment.show(getActivity().getSupportFragmentManager(),
                StatusDialogFragment.TAG + "_view");
    }

    // Show something when "Ping" is selected on overflow menu
    private void handlePingAction(int pos) {
        if(getActivity() != null && getActivity() instanceof MainActivity) {
            MessageDialogFragment messageDialogFragment = MessageDialogFragment.newInstance(mSignInAccount.getDisplayName(),
                    mSignInAccount.getId(), mPersons.get(pos).getName(), mPersons.get(pos).getId());
            messageDialogFragment.show(getActivity().getSupportFragmentManager(), MessageDialogFragment.TAG);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int itemId = item.getItemId();
        int clickedAdapterItem = mPopupClickedItem;

        switch (itemId) {
            case R.id.action_info:
                handleInfoAction(clickedAdapterItem);
                return true;
            case R.id.action_ping:
                handlePingAction(clickedAdapterItem);
                return true;
            default:return false;
        }
    }

    @Override
    public void onIndividualItemClick(View v) {
        // It's not the best approach, but it'll do...
        mPopupClickedItem =
                mRecyclerView.getChildAdapterPosition((RelativeLayout)
                        v.getParent());

        if(v.getId() == R.id.person_list_popup) {
            PopupMenu popup = new PopupMenu(getActivity(), v);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.popup_person, popup.getMenu());

            // Set listeners
            popup.setOnDismissListener(this);
            popup.setOnMenuItemClickListener(this);

            // Show the popup
            popup.show();
        }
    }

    @Override
    public void onItemClick(View v) {

    }

    @Override
    public void onItemLongClick(View v) {

    }

    @Override
    public void onDismiss(PopupMenu menu) {
        mPopupClickedItem = -1;
    }

    private void requestDataForAdapter(final GoogleSignInAccount googleSignInAccount) {
        if(googleSignInAccount == null) return;

        new SimpleAsync<Void, List<Person>, Void>(getActivity(), this) {
            @Override
            protected List<Person> process(Void input) {
                NearbyUserGetter nearbyUserGetter = new NearbyUserGetter(mContext);
                List<Person> persons = new ArrayList<>();

                try {
                    List<Person> apiPeople = nearbyUserGetter.load(Config.Api.GET,
                            (Config.Api.GET_NEARBY + googleSignInAccount.getId()));

                    if(apiPeople != null) {
                        persons.addAll(apiPeople);
                    }
                } catch (JSONException | LoaderException e) {
                    e.printStackTrace();
                }

                return persons;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onContentLoaderTaskFinished(List<Person> output) {
        if(getActivity() == null) return;

        // Hide progress bar
        if(mProgressBar != null)
            mProgressBar.setVisibility(View.GONE);

        // Output was null, probably server error
        if(output == null) output = new ArrayList<>();

        // Update adapter data
        mPersons = new ArrayList<>(output);

        // Setup adapter for real
        mPersonAdapter.updateData(output);

        // Handle if nothing was returned
        TextView textView = (TextView) getActivity().findViewById(R.id.people_empty);
        if(output.size() == 0) {
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }

        // Update nearby text view
        textView = (TextView) getActivity().findViewById(R.id.map_bottom_sheet_handle_text);
        if(textView != null) {
            textView.setText((mPersons == null ? 0 : mPersons.size()) + " nearby users");
        }

        if(getActivity().getIntent() != null && !handledMessageAction) {
            if(getActivity().getIntent().getAction() != null &&
                    getActivity().getIntent().getAction().equals(GoogleCloudMessagingService.ACTION_REPLY_TO_MESSAGE)) {

                String id = getActivity().getIntent().getStringExtra("replyTo");

                if(id == null) {
                    Log.d(TAG, "Action was set, but no replyTo extra");
                    return;
                }

                for(int i = 0; i < mPersons.size(); i++) {
                    if(mPersons.get(i).getId().equals(id)) {
                        handlePingAction(i);
                    }
                }
                handledMessageAction = true;
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onLoggedIn(boolean success, GoogleSignInAccount signInAccount) {
        requestDataForAdapter(signInAccount);

        mSignInAccount = signInAccount;
    }

    @Override
    public void onLocationConnected(Location location) {

    }

    @Override
    public void onRefreshRequested(GoogleSignInAccount signInAccount, Location location) {
        //Log.d(TAG, "Sync");
        requestDataForAdapter(signInAccount);
    }

    @Override
    public void onStop() {
        super.onStop();

        handledMessageAction = false;
    }

    @Override
    public void onPause() {
        super.onPause();

        handledMessageAction = false;
    }
}
