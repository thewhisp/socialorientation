package org.davis.socialorientation;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.location.LocationServices;
import com.pubnub.api.Callback;
import com.pubnub.api.PnGcmMessage;
import com.pubnub.api.PnMessage;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;
import com.pubnub.api.PubnubException;

import org.davis.socialorientation.config.Config;
import org.davis.socialorientation.containers.Person;
import org.davis.socialorientation.database.preferences.SharedPreferencesHelper;
import org.davis.socialorientation.fragments.containers.FragmentHome;
import org.davis.socialorientation.fragments.dialogs.StatusDialogFragment;
import org.davis.socialorientation.loaders.UserDataPoster;
import org.davis.socialorientation.tasks.SimpleAsync;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by Dāvis Mālnieks on 04/04/16.
 */


/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class MainActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback {

    private static final String TAG = "MainActivity";

    private static final int REQUEST_LOGIN = 62;

    private static final String SENDER_ID = "363309312815";

    public static final int LOCATION_PERSMISSION_REQUEST = 37;

    // Google

    public GoogleSignInAccount mGoogleSignInAccount;

    GoogleApiClient mGoogleApiClient;

    GoogleCloudMessaging mGCM;

    public Location mLastLocation;

    String mRegistrationID;

    // PubNub

    private final Pubnub mPubnub = new Pubnub("pub-c-c7ac4355-4f52-494d-a2be-82f34763e416",
            "sub-c-eeba82a2-2754-11e6-be83-0619f8945a4f");

    String mPubNubChannel;

    // UI

    protected DrawerLayout mDrawerLayout;

    protected NavigationView mDrawerNavigationView;

    protected Toolbar mToolbar;

    // Listeners

    ArrayList<ActivityFragmentBridge> mFragmentBridge;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init content
        setContentView(R.layout.activity_main);

        // Get the drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Get the drawer's navigation view
        mDrawerNavigationView = (NavigationView) findViewById(R.id.drawer_navigation);

        mDrawerNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if (menuItem.isChecked()) return false;

                switch (menuItem.getItemId()) {
                    case R.id.drawer_status:
                        StatusDialogFragment statusDialogFragment = new StatusDialogFragment();
                        // Add and show the fragment
                        statusDialogFragment.show(getSupportFragmentManager(),
                                StatusDialogFragment.TAG);
                        return true;
                    case R.id.drawer_settings:
                        Intent intent = new Intent();

                        intent.setClass(mDrawerNavigationView.getContext(), SettingsActivity.class);
                        startActivity(intent);
                        return true;
                }

                return false;
            }
        });
        mDrawerNavigationView.getMenu().findItem(R.id.drawer_status).setChecked(false);

        // Create an instance of GoogleAPIClient for location
        if (mGoogleApiClient == null) {
            // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(com.google.android.gms.appindexing.AppIndex.API).build();
        }

        if(hasLocationPermission(this)) {
            // Call onCreate
            onCreateCalled(savedInstanceState);

            // Get sign in details
            performLogin();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION
                    },
                    LOCATION_PERSMISSION_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if(requestCode == LOCATION_PERSMISSION_REQUEST) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Call onCreate
                onCreateCalled(null);

                // Get sign in details
                performLogin();
            } else {
                Toast.makeText(this, "This app needs to access your location to function!",
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    public static boolean hasLocationPermission(final Context context) {
        return !(ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED);
    }

    public void addOnBackPressedListener(ActivityFragmentBridge activityFragmentBridge) {
        if(mFragmentBridge == null)
            mFragmentBridge = new ArrayList<>();

        mFragmentBridge.add(activityFragmentBridge);
    }

    public void onCreateCalled(Bundle savedInstanceState) {
        // Create the main fragment
        if (savedInstanceState == null) {
            FragmentHome fragmentHome = new FragmentHome();

            // Add and show the fragment
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.activity_main_fragment_container, fragmentHome, FragmentHome.TAG)
                    .commitAllowingStateLoss();

            // Back-stack listener
            getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    /* Stub */
                }
            });
        }
    }

    // Performs the login
    private void performLogin() {
        Log.d(TAG, "Redirecting to login activity...");

        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, REQUEST_LOGIN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "Received result from login activity, request code: " + requestCode);

        if(requestCode == REQUEST_LOGIN) {
            if(resultCode == LoginActivity.LOGIN_GOOGLE_OK) {
                Log.d(TAG, "Successfully logged in");
                mGoogleSignInAccount = data.getParcelableExtra(LoginActivity.EXTRA_SIGN_IN_ACCOUNT);
                onLoggedIn(true);
            } else {
                onLoggedIn(false);
            }
        }
    }

    // User successfully logged in
    public void onLoggedIn(boolean success) {
        TextView loggedInAs = (TextView)
                mDrawerNavigationView.getHeaderView(0).findViewById(R.id.drawer_header_title);

        if(loggedInAs == null) return;

        if(success) {
            loggedInAs.setText(mGoogleSignInAccount.getDisplayName());
        } else {
            Toast.makeText(this, "Could not sign in", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        for(ActivityFragmentBridge o : mFragmentBridge) {
            if(o != null) {
                o.onLoggedIn(success, mGoogleSignInAccount);
            }
        }

        // Register with GCM
        mPubNubChannel = mGoogleSignInAccount.getId();
        registerWithGcm();
    }

    // Setups toolbar
    public void setToolbar(Toolbar toolbar, boolean homeAsUp) {
        if(toolbar == null) throw new IllegalArgumentException("Toolbar cannot be null");

        mToolbar = toolbar;
        setSupportActionBar(mToolbar);

        if(getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(homeAsUp);

        // Re-create menu
        invalidateOptionsMenu();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if(!MainActivity.hasLocationPermission(this)) return;

        // Get location
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        for(ActivityFragmentBridge o : mFragmentBridge) {
            if(o != null) {
                o.onLocationConnected(mLastLocation);
            }
        }

        // Update user data on the cloud
        new SimpleAsync<Void, String, Void>(this) {

            @Override
            protected String process(Void input) {
                Log.d(TAG, "About to post user data");

                if(mLastLocation == null || mGoogleSignInAccount == null) {
                    Log.d(TAG, "No google sign in or location, stop");
                    return null;
                }

                // Data
                Person localUser = new Person(mGoogleSignInAccount.getDisplayName(), mGoogleSignInAccount.getId(),
                        mLastLocation.getLongitude(), mLastLocation.getLatitude(),
                        -1, // TODO
                        SharedPreferencesHelper.getStringPreference(mContext,
                                SharedPreferencesHelper.Keys.USER_STATUS, null), true);

                List<Person> data = new ArrayList<>();
                data.add(localUser);
                UserDataPoster userDataPoster = new UserDataPoster(mContext);
                return userDataPoster.post(Config.Api.POST, data);
            }

            @Override
            protected void onLoadFinished(String output) {
                if(output != null && (output.contains("error") || output.contains("exception"))) {
                    Log.d(TAG, "Error: " + output);
                    Toast.makeText(mContext, "Could not update your data. General error.",
                            Toast.LENGTH_SHORT).show();
                } else if(output != null && output.equals("failed")) {
                    Toast.makeText(mContext, "Could not update your data. Server error.",
                            Toast.LENGTH_SHORT).show();
                } else if(output != null) {
                    Log.d(TAG, "Success: " + output);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private void registerWithGcm() {
        Log.d(TAG, "Registering with GCM");

        mGCM = GoogleCloudMessaging.getInstance(this);
        try {
            mRegistrationID = getRegistrationId();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mRegistrationID == null || mRegistrationID.isEmpty()) {
            registerInBackground();
        } else {
            Log.d(TAG, "Already registered with GCM");
        }
    }

    // Unregister
    private void unregisterFromGcm() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    if (mGCM == null) {
                        mGCM = GoogleCloudMessaging.getInstance(MainActivity.this);
                    }

                    // Unregister from GCM
                    mGCM.unregister();

                    // Remove Registration ID from memory
                    storeRegistrationId(null);

                    // Disable Push Notification
                    mPubnub.disablePushNotificationsOnChannel(mPubNubChannel, mRegistrationID);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute(null, null, null);
    }

    // Handle registration on background thread (since it involves using network)
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
                String msg;
                try {
                    if (mGCM == null) {
                        mGCM = GoogleCloudMessaging.getInstance(MainActivity.this);
                    }
                    mRegistrationID = mGCM.register(SENDER_ID);
                    msg = "Device registered, registration ID: " + mRegistrationID;

                    sendRegistrationId(mRegistrationID);
                    storeRegistrationId(mRegistrationID);
                    Log.i(TAG, msg);
                } catch (Exception ex) {
                    msg = "Error :" + ex.getMessage();
                    Log.e(TAG, msg);
                }
                return msg;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    // Send regisration id to PubNub
    private void sendRegistrationId(String regId) {
        mPubnub.enablePushNotificationsOnChannel(
                mPubNubChannel,
                regId);
    }

    // Send notification (message)
    public void sendNotification(String sender, String senderId,
                                        String receiverId, String msg) {
        PnGcmMessage gcmMessage = new PnGcmMessage();

        JSONObject jso = new JSONObject();
        try {
            jso.put("sender", sender);
            jso.put("message", msg);
            jso.put("senderId", senderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        gcmMessage.setData(jso);

        PnMessage message = new PnMessage(
                mPubnub,
                receiverId,
                callback,
                gcmMessage);
        try {
            message.publish();
        } catch (PubnubException e) {
            e.printStackTrace();
        }
    }

    private static Callback callback = new Callback() {
        @Override
        public void successCallback(String channel, Object message) {
            Log.d(TAG, "Message sent. " + channel + " : " + message);
        }
        @Override
        public void errorCallback(String channel, PubnubError error) {
            Log.d(TAG, "Message not sent. " + channel + " : " + error);
        }
    };

    // Get the registration id from settings
    private String getRegistrationId() throws Exception {
        return SharedPreferencesHelper.getStringPreference(this,
                SharedPreferencesHelper.Keys.REG_ID, null);
    }

    // Save registration id to settings
    private void storeRegistrationId(String id) {
        SharedPreferencesHelper.saveStringPreference(this,
                SharedPreferencesHelper.Keys.REG_ID, id);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if(mFragmentBridge == null) {
            super.onBackPressed();
            return;
        }

        boolean handled = false;
        for(ActivityFragmentBridge o : mFragmentBridge) {
            if(o != null) {
                if(!handled)
                    handled = o.onBackPressed();
            }
        }

        if(!handled) super.onBackPressed();
    }

    /*
        ACTIVITY LIFETIME
     */

    @Override
    protected void onStart() {
        super.onStart();
        if(mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResult(Result result) {

    }

    public interface ActivityFragmentBridge {
        boolean onBackPressed();

        void onLoggedIn(boolean success, GoogleSignInAccount signInAccount);

        void onLocationConnected(Location location);
    }
}
