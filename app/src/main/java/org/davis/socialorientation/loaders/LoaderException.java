package org.davis.socialorientation.loaders;

/*
 * Created by davis on 17/05/16.
 */

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class LoaderException extends Exception {
    public static final String ERR_NO_CONNECTION = "No connection";

    public static final String ERR_CANNOT_REACH_HOST = "Cannot connect to host";

    public static final String ERR_CHECK_CONNECTION = "Check you internet connection";

    public static final String ERR_NOTHING_FOUND = "Nothing found";

    public static final String ERR_CANNOT_PARSE = "Error loading data";

    public LoaderException(String exception) {
        super(exception);
    }
}
