package org.davis.socialorientation.loaders;

import android.content.Context;
import android.util.Log;

import org.davis.socialorientation.config.Config;
import org.davis.socialorientation.utils.NetworkUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by davis on 25/05/16.
 */
public class Utils {

    /* Checks if there is an active connection */
    public static void checkConnection(final Context context,
                                   final String serviceUrl) throws LoaderException {
        if(NetworkUtils.currentConnectedNetworkType(context) == -1) {
            throw new LoaderException(LoaderException.ERR_NO_CONNECTION);
        } else if(!NetworkUtils.isHostOnline(serviceUrl, 80, Config.Network.HOST_TIMEOUT)) {
            if(NetworkUtils.isHostOnline("http://google.com", 80, Config.Network.HOST_TIMEOUT))
                throw new LoaderException(LoaderException.ERR_CANNOT_REACH_HOST);
            else throw new LoaderException(LoaderException.ERR_CHECK_CONNECTION);
        }
    }

    /* Creates error message in JSON format */
    public static JSONObject createErrorMessage(String message,
                                                String localizedExceptionMessage) {
        JSONObject errorObject = new JSONObject();

        // Set error
        try {
            errorObject.put("error", message);
            errorObject.put("exception", localizedExceptionMessage);
        } catch (JSONException e1) {
            Log.e("Loaders.Utils", "Error creating error");
            return null;
        }

        return errorObject;
    }
}
