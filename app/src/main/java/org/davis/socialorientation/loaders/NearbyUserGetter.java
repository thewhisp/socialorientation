package org.davis.socialorientation.loaders;

import android.content.Context;
import android.util.Log;

import org.davis.socialorientation.containers.Person;
import org.davis.socialorientation.loaders.base.Getter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by davis on 24/05/16.
 */

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class NearbyUserGetter extends Getter<Person> {

    public NearbyUserGetter(final Context context) {
        super(context);
    }

    @Override
    protected List<Person> load_internal(String json)
            throws JSONException, LoaderException {

        // NOTICE: Any exceptions thrown here can be detect on "onLoadFinished"
        // NOTICE:  ... if parameter "msg" is null, it means there was no error
        //Log.d(TAG, "Loading nearby users, json data: " + json);

        // Get root of the document
        JSONArray persons = new JSONArray(json);

        // Result collection
        List<Person> result = new ArrayList<>();

        // Parse json
        int notLoadedItems = 0;
        JSONObject person;
        for(int i = 0; i < persons.length(); i++) {
            try {
                person = persons.getJSONObject(i);

                result.add(new Person(
                        person.getString("Name"),
                        person.getString("googleID"),
                        person.getDouble("longitude"),
                        person.getDouble("latitude"),
                        person.getInt("status"),
                        person.getString("Message"),
                        false,
                        person.getDouble("0"),  // Distance, api returns weird key
                        person.getString("Datums")));
            } catch (JSONException e) {
                e.printStackTrace();
                notLoadedItems++;
            }
        }

        if(notLoadedItems > 0)
            Log.w(TAG, "Could not load " + notLoadedItems + " person data");

        return result;
    }

    @Override
    protected void onLoadFinished(List<Person> result, String msg) {
        //Log.d(TAG, "Nearby user load finished - " + (msg == null ? " no error" : msg));
    }

}

