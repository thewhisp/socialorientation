package org.davis.socialorientation.loaders;

import android.content.Context;
import android.util.Log;

import org.davis.socialorientation.containers.Person;
import org.davis.socialorientation.fragments.FragmentSettings;
import org.davis.socialorientation.loaders.base.Poster;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/*
 * Created by Dāvis Mālnieks on 17/05/2016.
 */

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class UserDataPoster extends Poster<Person, String> {

    String mMethod = "pushLocalUserData";

    public UserDataPoster(final Context context) {
        super(context);
    }

    public void setMethod(String method) {
        this.mMethod = method;
    }

    @Override
    protected JSONObject parseResult(JSONObject result) {
        return result;
    }

    @Override
    protected String convertToEndResult(JSONObject parsedData) {
        String error = "could not get status, failed to parse", exception = "unknown";
        try {
            error = parsedData.getString("error");
            exception = parsedData.getString("exception");
        } catch (JSONException e) {
            try {
                return parsedData.getString("status");
            } catch (JSONException e1) {
                Log.w(TAG, "Shouldn't have happened");
            }
        }

        return "error:" + error + "|" + "exception:" + exception;
    }

    @Override
    protected String jsonize(List<Person> items) throws JSONException {
        JSONArray rootArray = new JSONArray();
        JSONObject arrayItem;

        // Add method name for API
        rootArray.put(getMethodHeader());

        for(Person person : items) {
            arrayItem = new JSONObject();
            arrayItem.put("name", person.getName());
            arrayItem.put("id", person.getId());
            arrayItem.put("status", person.getStatus());
            arrayItem.put("message", person.getMessage());
            arrayItem.put("image_profile", person.getProfileImagePath());
            arrayItem.put("image_google", person.getGoogleImagePath());
            arrayItem.put("longitude", person.getLongitude());
            arrayItem.put("latitude", person.getLatitude());
            arrayItem.put("localUser", person.getIsLocalUser());

            if(person.getIsLocalUser()) {
                arrayItem.put("radius", FragmentSettings.getZoneRadius(mContext));
            }

            // Add to array
            rootArray.put(arrayItem);
        }

        return rootArray.toString();
    }

    // Method that tells API which method to use
    protected JSONObject getMethodHeader() throws JSONException {
        JSONObject header = new JSONObject();
        header.put("method", mMethod);

        return header;
    }
}
