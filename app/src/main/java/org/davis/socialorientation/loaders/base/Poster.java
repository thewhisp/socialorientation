package org.davis.socialorientation.loaders.base;

import android.content.Context;
import android.util.Log;

import org.davis.socialorientation.containers.Container;
import org.davis.socialorientation.loaders.LoaderException;
import org.davis.socialorientation.loaders.Utils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/*
 * Created by Dāvis Mālnieks on 17/05/2016.
 */

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public abstract class Poster<T extends Container, R> {

    public static final String TAG = "Poster";

    public static final boolean DEBUG = false;

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    protected Context mContext;

    public Poster(final Context context) {
        this.mContext = context;
    }

    // Initiates the post
    public R post(final String serviceUrl,
                                    List<T> data) {
        return convertToEndResult(post_internal(serviceUrl, data));
    }

    // Parse the result
    protected abstract JSONObject parseResult(JSONObject result);

    // Parse the result
    protected abstract R convertToEndResult(JSONObject parsedData);

    // Convert data to JSON
    protected abstract String jsonize(List<T> items) throws JSONException;

    protected JSONObject post_internal(String serviceUrl, List<T> data) {
        // Check connection
        try {
            Utils.checkConnection(mContext, serviceUrl);
        } catch (LoaderException e) {
            return Utils.createErrorMessage("Network connection failed", e.getLocalizedMessage());
        }

        OkHttpClient client = new OkHttpClient();

        // Convert data to json
        String requestJson;
        try {
            requestJson = jsonize(data);
            Log.d(TAG, "JSON: " + requestJson);
        } catch (JSONException e) {
            return Utils.createErrorMessage("Could not convert data to JSON", e.getLocalizedMessage());
        }

        // Send request
        RequestBody body = RequestBody.create(JSON, requestJson);
        Request request = new Request.Builder()
                .url(serviceUrl)
                .post(body)
                .build();

        // Get response
        Response response;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            return Utils.createErrorMessage("Could not make request", e.getLocalizedMessage());
        }

        JSONObject jsonResponse;
        try {
            jsonResponse = new JSONObject();
            jsonResponse.put("status", response.body().string());
        } catch (JSONException e) {
            return Utils.createErrorMessage("Could not parse request result", e.getLocalizedMessage());
        } catch (IOException e) {
            return Utils.createErrorMessage("Could not parse request result", e.getLocalizedMessage());
        }

        return parseResult(jsonResponse);
    }
}
