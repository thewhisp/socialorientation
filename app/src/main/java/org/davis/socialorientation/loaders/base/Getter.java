package org.davis.socialorientation.loaders.base;

import android.content.Context;
import android.util.Log;

import org.davis.socialorientation.loaders.LoaderException;
import org.davis.socialorientation.loaders.Utils;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/*
 * Created by davis on 24/05/16.
 */

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public abstract class Getter<T> {

    public static final String TAG = "Getter";

    Context mContext;

    public Getter(final Context context) {
        this.mContext = context;
    }

    // Each class that extends this need to overload this method
    protected abstract List<T> load_internal(String json) throws JSONException, LoaderException;

    public List<T> load(final String serviceUrl, final String query)
            throws JSONException, LoaderException {
        List<T> result = null;
        try {
            Utils.checkConnection(mContext, serviceUrl);
            result = load_internal(getJSON(serviceUrl, query));
        } catch (Exception e) {
            onLoadFinished(result, e.getLocalizedMessage());
            return null;
        }

        onLoadFinished(result, null);
        return result;
    }

    protected abstract void onLoadFinished(List<T> result, String msg);

    // Gets the JSON data
    protected String getJSON(String serviceUrl, String query) {
        if(query == null) {
            query = "";
        }

        HttpURLConnection connection = null;
        InputStream inputStream = null;

        try {
            // If query has spaces, encode it as proper url
            //query = URLEncoder.encode(query, "UTF-8"); todo

            URL dURL = new URL(serviceUrl + query);
            connection = (HttpURLConnection) dURL.openConnection();
            connection.setRequestMethod("GET");
            inputStream = connection.getInputStream();

            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream,
                    "UTF-8"), 8);
            StringBuilder sBuilder = new StringBuilder();

            String line;
            while ((line = bReader.readLine()) != null) {
                sBuilder.append(line).append("\n");
            }

            inputStream.close();
            return sBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }

            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Log.e(TAG, "Could not close input stream: " + e.getLocalizedMessage());
                }
            }
        }

        return null;
    }

}
