package org.davis.socialorientation.tasks;

/*
 * Created by Dāvis Mālnieks on 29/04/2016
 */

import android.content.Context;

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */

// TODO: Make this something like service call which can also return result code from servcie (success, fail, no connection etc)
public abstract class SimpleAsync<I, O, T> extends BitmapLoader<I, O, T> {

    ContentLoaderTaskListener<O> mContentLoaderTaskListener;

    public SimpleAsync(Context context) {
        super(context, null, null);
    }

    public SimpleAsync(Context context, ContentLoaderTaskListener<O> taskListener) {
        super(context, null, null);

        this.mContentLoaderTaskListener = taskListener;
    }

    @Override
    protected void onPostExecute(O output) {
        if(mContentLoaderTaskListener != null) {
            mContentLoaderTaskListener.onContentLoaderTaskFinished(output);
        } else {
            onLoadFinished(output);
        }
    }

    @Override
    protected void applyOutputToTarget(O output, T target) {
        // Don't ask to override this
    }

    // Called when the load task is finished
    protected void onLoadFinished(O output) {
        // It's not a "must" to implement this, so don't make it abstract
    }

    public interface ContentLoaderTaskListener<O> {
        void onContentLoaderTaskFinished(O output);
    }
}
