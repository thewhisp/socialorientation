package org.davis.socialorientation.tasks;

/*
 * Created by Dāvis Mālnieks on 27/05/2015
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

import org.davis.socialorientation.config.Config;
import org.davis.socialorientation.utils.cache.CacheWrapper;
import org.davis.socialorientation.utils.cache.bitmap.BitmapUtils;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;

/*
    This task is used to load multiple bitmaps, i,e,  into adapters
 */
public class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {

    private final WeakReference<ImageView> imageViewReference;

    private String mPath = "";

    private Context mContext;

    private static CacheWrapper mCache;

    private int[] mSize = {
            Config.Ui.Image.SIZE_DEFAULT,
            Config.Ui.Image.SIZE_DEFAULT
    };

    public BitmapWorkerTask(final ImageView imageView, int size[]) {

        if(mCache == null) {
            try {
                mCache = new CacheWrapper(imageView.getContext(),
                        false, "_".concat(String.valueOf(size[0])));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            mCache.mSizePrefix = "_".concat(String.valueOf(size[0]));
        }

        this.mSize = size;

        // Use a WeakReference to ensure the ImageView can be garbage collected
        imageViewReference = new WeakReference<>(imageView);
    }

    public BitmapWorkerTask(final ImageView imageView, int size[], boolean fromAlbumID) {
        this(imageView, size);

        if(fromAlbumID) {
            this.mContext = imageView.getContext();
        }

    }

    // Starts the task and puts the place holder image while it loads
    public static void loadBitmap(String path, ImageView imageView,
                                  Bitmap placeHolder, int[] size) {

        if (cancelPotentialWork(path, imageView)) {
            final BitmapWorkerTask task = new BitmapWorkerTask(imageView, size);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(placeHolder, task);

            imageView.setImageDrawable(asyncDrawable);
            task.execute(path);
        }
    }

    // Starts the task and puts the place holder image while it loads
    public static void loadBitmap(long albumID, ImageView imageView,
                                  Bitmap placeHolder, int[] size) {

        String path = String.valueOf(albumID);

        if (cancelPotentialWork(path, imageView)) {
            final BitmapWorkerTask task = new BitmapWorkerTask(imageView, size, true);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(placeHolder, task);

            imageView.setImageDrawable(asyncDrawable);
            task.execute(path);
        }
    }

    /*
     * CACHE
     */

    // Decode image in background.
    @Override
    protected Bitmap doInBackground(String... params) {
        mPath = params[0];
        if (mPath == null || !new File(mPath).exists()) {
            return null;
        }

        Bitmap resultBitmap = null;
        try {
            // Try to get bitmap from cache
            resultBitmap = mCache.getBitmapFromCache(mPath);

            // If it's null, it's not in cache
            if(resultBitmap != null) {
                return resultBitmap;
            }

            resultBitmap = BitmapUtils.decodeSampledBitmap(mPath, mSize[0], mSize[1]);

            // Add to cache
            mCache.addBitmapToCache(mPath, resultBitmap);
        } catch (Exception e) {
            // Couldn't load from cache, or path was null
        }

        return resultBitmap;
    }

    // Once complete, see if ImageView is still around and set bitmap.
    @Override
    protected void onPostExecute(Bitmap result) {
        if (isCancelled()) {
            result = null;
        }

        if (result != null) {
            final ImageView imageView = imageViewReference.get();

            final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

            if (this == bitmapWorkerTask) {
                imageView.setImageBitmap(result);
            }
        }
    }

    // Gets bitmap worker task
    private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();

            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
                return asyncDrawable.getBitmapWorkerTask();
            }
        }

        return null;
    }

    // If bitmapData is not yet set or it differs from the new data, then...
    private static boolean cancelPotentialWork(String data, ImageView imageView) {
        final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

        if (bitmapWorkerTask != null) {
            final String bitmapData = bitmapWorkerTask.mPath;

            if (bitmapData == null || !bitmapData.equals(data)) {
                // ... cancel previous task
                bitmapWorkerTask.cancel(true);
            } else {
                // ... the same work is already in progress
                return false;
            }
        }

        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }

    // AsyncDrawable used to put place holder
    private static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

        public AsyncDrawable(Bitmap bitmap, BitmapWorkerTask bitmapWorkerTask) {
            super(bitmap);
            bitmapWorkerTaskReference = new WeakReference<>(bitmapWorkerTask);
        }

        public BitmapWorkerTask getBitmapWorkerTask() {
            return bitmapWorkerTaskReference.get();
        }
    }
}