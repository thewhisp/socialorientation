package org.davis.socialorientation.tasks;

/*
 * (C) akhyrul
 *
 * https://gist.github.com/akhyrul/6317789
 */

import android.content.Context;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;

/*
    This task is used to load single bitmap
 */
public abstract class BitmapLoader<I, O, T> extends AsyncTask<Void, Void, O> {

    protected final WeakReference<T> mTargetReference;

    protected I mInput;

    protected Context mContext;

    public BitmapLoader(final Context context, I input, T target) {
        this.mContext = context;
        this.mInput = input;
        mTargetReference = new WeakReference<>(target);
    }

    public BitmapLoader(I input, T target) {
        this.mInput = input;
        mTargetReference = new WeakReference<>(target);
    }

    @Override
    protected O doInBackground(Void... args) {
        return process(mInput);
    }

    protected abstract O process(I input);

    @Override
    protected void onPostExecute(O output) {
        final T target = mTargetReference.get();
        if (target != null)
            applyOutputToTarget(output, target);
    }

    protected abstract void applyOutputToTarget(O output, T target);
}
