package org.davis.socialorientation.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.TypedValue;

import org.davis.socialorientation.R;

/*
 * Created by davis on 23/05/16.
 */

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public final class UiUtils {

    // Gets accent color
    public static int getAccentColor(final Context context) {
        TypedValue typedValue = new TypedValue();

        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[]
                {R.attr.colorAccent});
        int color = a.getColor(0, 0);

        a.recycle();

        return color;
    }
}
