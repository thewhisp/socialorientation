package org.davis.socialorientation.utils;

import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by davis on 03/06/16.
 */

public final class TimeUtils {

    // 2016-06-03 21:32:26
    @Nullable
    public static Date parseLastActivityDate(String date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.UK);

        Date result;
        try {
            result = format.parse(date);
        } catch (ParseException e) {
            Log.w("TimeUtils", "Failed to parse date: " + date);
            return null;
        }

        return result;
    }

    public static Pair<Long, TimeUnit> getLastActivity(Date date) {
        Date current = new Date();
        long duration = current.getTime() - date.getTime();

        // Seconds
        long diff = TimeUnit.MILLISECONDS.toSeconds(duration);
        if(diff < 60)
            return new Pair<>(diff, TimeUnit.SECONDS);

        // Minutes
        diff = TimeUnit.MILLISECONDS.toMinutes(duration);
        if(diff < 60)
            return new Pair<>(diff, TimeUnit.MINUTES);

        // Hours
        diff = TimeUnit.MILLISECONDS.toHours(duration);
        if(diff < 60)
            return new Pair<>(diff, TimeUnit.HOURS);

        // Days
        diff = TimeUnit.MILLISECONDS.toDays(duration);
        return new Pair<>(diff, TimeUnit.DAYS);
    }
}
