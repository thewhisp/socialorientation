package org.davis.socialorientation.utils.cache;

/*
 * Created by Dāvis Mālnieks on 03/06/2015
 */

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.util.LruCache;

import com.jakewharton.disklrucache.DiskLruCache;

import java.io.IOException;
import java.io.OutputStream;

public final class CacheWrapper {

    public static final String TAG = "CacheWrapper";

    public static final boolean DEBUG = false;

    // Memory cache
    private static LruCache<String, Bitmap> cache = null;

    // Disk cache
    private static DiskLruCache diskCache = null;

    // Use memory cache?
    private boolean mUseMemoryCache;

    // Size prefix
    public String mSizePrefix;

    public CacheWrapper(final Context context, boolean useMemCache,
                        String sizePrefix) throws IOException {

        openDiskCache(context);

        if(!useMemCache) {
            openMemoryCache(context);
        }

        this.mUseMemoryCache = useMemCache;
        this.mSizePrefix = sizePrefix;
    }

    // Open cache
    public static synchronized void openDiskCache(final Context context) throws IOException {
        if (diskCache == null) {
            diskCache = DiskLruCache.open(context.getCacheDir(), 1, 1, 1024 * 1024 * 10);
        }
    }

    // Open memory cache
    public static synchronized void openMemoryCache(final Context context) {
        if (cache == null) {
            final int memClass = ((ActivityManager) context.getSystemService(
                    Context.ACTIVITY_SERVICE)).getMemoryClass();

            cache = new LruCache<String, Bitmap>(1024 * 1024 * memClass / 4) {
                @Override
                protected int sizeOf(String key, Bitmap value) {
                    return value.getRowBytes() * value.getHeight();
                }
            };
        }
    }

    // Adds bitmap to cache
    public void addBitmapToCache(String key, Bitmap bitmap) {
        if(key == null || key.length() == 0) {
            key = "default";
        }
        key = key.concat(mSizePrefix);

        if(DEBUG) Log.d(TAG, "Adding to cache, key is: " + key);

        if (getBitmapFromMemCache(key) == null && mUseMemoryCache) {
            addBitmapToMemoryCache(key, bitmap);
        }

        if (getBitmapFromDiskCache(key) == null) {
            addBitmapToDiskCache(bitmap, key);
        }
    }

    // Gets bitmap from either memory cache or disk cache
    public Bitmap getBitmapFromCache(String key) {
        Bitmap bitmap = null;

        if(key == null || key.length() == 0) {
            key = "default";
        }
        key = key.concat(mSizePrefix);

        if(DEBUG) Log.d(TAG, "Getting from cache, key is: " + key);

        if(mUseMemoryCache) bitmap = getBitmapFromMemCache(key);

        if (bitmap == null) {
            bitmap = getBitmapFromDiskCache(key);
        }
        return bitmap;
    }

    // Adds bitmap to memory cache
    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            cache.put(key, bitmap);
        }
    }

    // Adds bitmap to disk cache
    public void addBitmapToDiskCache(Bitmap bitmap, String key) {
        try {
            DiskLruCache.Editor editor = diskCache.edit(key.hashCode() + "");
            if (editor != null) {
                OutputStream os = editor.newOutputStream(0);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                editor.commit();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Bitmap getBitmapFromDiskCache(String key) {
        Bitmap bitmap = null;
        try {
            DiskLruCache.Snapshot snapshot = diskCache.get(key.hashCode() + "");
            if (snapshot != null) {
                bitmap = BitmapFactory.decodeStream(snapshot.getInputStream(0));
            }
        } catch (IOException e) {
            bitmap = null;
        }

        return bitmap;
    }

    // Gets bitmap from memory cache
    public Bitmap getBitmapFromMemCache(String key) {
        return cache.get(key);
    }

    // Release the static references
    public void unload() {
        diskCache = null;
        if(mUseMemoryCache) cache = null;
    }
}
