package org.davis.socialorientation.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URL;
import java.net.UnknownHostException;

/*
 * Created by davis on 17/05/16.
 */

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public final class NetworkUtils {

    public static final int NETWORK_NONE = 0;

    public static final int NETWORK_MOBILE = 1;

    public static final int NETWORK_WIFI = 2;

    public static final int NETWORK_MOBILE_AND_WIFI = 3;

    // Checks if a host is online (MUST BE RUN ON separate thread)
    public static boolean isHostOnline(String url, int port, int timeout) {
        boolean reachable;

        InetAddress address;
        try {
            address = InetAddress.getByName(new URL(url).getHost());
        } catch (UnknownHostException | MalformedURLException e) {
            Log.e("NetworkUtils", "Invalid address or unknown host");
            return false;
        }

        // Get the IP
        String ip = address.getHostAddress();

        Socket socket = null;
        try {
            SocketAddress socketAddress = new InetSocketAddress(ip, port);
            // Create an unbound socket
            socket = new Socket();

            // This method will block no more than timeoutMs.
            // If the timeout occurs, SocketTimeoutException is thrown.
            socket.connect(socketAddress, timeout);
            reachable = true;
        } catch(Exception e) {
            reachable = false;
        } finally {
            if(socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    Log.i("NetworkUtils", "Couldn't close socket");
                }
            }
        }

        return reachable;
    }

    /*
        Checks the current network type (MOBILE 0 or WIFI 1)
        Returns -1 if neither are available
    */
    public static int currentConnectedNetworkType(final Context context) {
        int type = NETWORK_NONE; // no network

        ConnectivityManager mConnManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo.State mobile_state =
                mConnManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState();
        NetworkInfo.State wifi_state =
                mConnManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();

        if(mobile_state == NetworkInfo.State.CONNECTED
                || mobile_state == NetworkInfo.State.CONNECTING) {
            type += NETWORK_MOBILE;
        }

        if(wifi_state == NetworkInfo.State.CONNECTED
                || wifi_state == NetworkInfo.State.CONNECTING) {
            type += NETWORK_WIFI;
        }

        return type;
    }

}
