package org.davis.socialorientation.config;

/**
 * Created by davis on 06/04/16.
 */

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public final class Config {

    public static final class Api {
        public static final String POST = "http://kratuve.va.lv/orientesanas/ievada.php";

        public static final String GET = "http://kratuve.va.lv/orientesanas/getData.php";

        public static final String GET_NEARBY = "?ID="; // NOTICE: ?nearby=[id] (id = requester)
    }

    // Preferences
    public static final class Prefs {
        public static final String MAIN_PREFERENCES = "main_preferences";

        public static final String LOGGED_IN = "logged_in";

        public static final String LOGGED_IN_GOOGLE = "logged_in_google";
    }

    // User interface
    public static final class Ui {
        /* Colors and stuff will go here */

        public static final class Image {
            public static final int SIZE_DEFAULT = 250;

            public static final int SIZE_PROFILE_LIST = 128;
        }
    }

    // Network
    public static final class Network {
        public static final int HOST_TIMEOUT = 2000; // in ms
    }

    public static final String DEVELOPER_LINK = "https://bitbucket.org/thewhisp/socialorientation";
}
