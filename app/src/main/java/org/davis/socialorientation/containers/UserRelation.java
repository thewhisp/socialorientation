package org.davis.socialorientation.containers;

/*
 * Created by davis on 25/05/16.
 */

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class UserRelation extends Container {

    boolean mRelated;

    String mDateAdded;

    int mCommonPeople;

    public UserRelation(boolean related, String dateAdded, int commonPeople) {
        this.mRelated = related;
        this.mDateAdded = dateAdded;
        this.mCommonPeople = commonPeople;
    }

    public boolean getIsRelated() { return mRelated; }

    public String getDateAdded() { return mDateAdded; }

    public int getCommonPeople() { return mCommonPeople; }
}
