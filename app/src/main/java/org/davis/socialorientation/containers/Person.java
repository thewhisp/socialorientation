package org.davis.socialorientation.containers;

/*
 * Created by Dāvis Mālnieks on 19/04/16.
 */

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class Person extends Container {
    String mName;

    String mID;

    double mLongitude, mLatitude;

    int mStatus;

    boolean mIsLocalUser = false;

    String mMessage;

    String mProfileImage;

    String mGoogleImage;

    double mDistance;

    String mDate;

    public Person(String name, String id,
                  double longitude, double latitude, int status, String message) {
        this.mName = name;
        this.mID = id;
        this.mLongitude = longitude;
        this.mLatitude = latitude;
        this.mStatus = status;
        this.mMessage = message;
    }

    public Person(String name, String id,
                  double longitude, double latitude, int status, String message,
                  boolean localUser) {
        this(name, id, longitude, latitude, status, message);

        this.mIsLocalUser = localUser;
    }

    public Person(String name, String id,
                  double longitude, double latitude, int status, String message,
                  boolean localUser, double distance, String date) {
        this(name, id, longitude, latitude, status, message);

        this.mIsLocalUser = localUser;
        this.mDistance = distance;
        this.mDate = date;
    }


    public String getName() { return mName; }

    public String getId() { return mID; }

    public double getLongitude() { return mLongitude; }

    public double getLatitude() { return mLatitude; }

    public int getStatus() { return this.mStatus; }

    public String getMessage() { return this.mMessage; }

    public boolean getIsLocalUser() { return this.mIsLocalUser; }

    public String getProfileImagePath() { return this.mProfileImage; }

    public String getGoogleImagePath() { return this.mGoogleImage; }

    public double getDistance() { return this.mDistance; }

    public String getDate() { return this.mDate; }
}
