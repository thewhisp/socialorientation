package org.davis.socialorientation;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.davis.socialorientation.config.Config;


/**
 * Created by Dāvis Mālnieks on 04/04/16.
 */

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class LoginActivity extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final String TAG = "SignInActivity";

    private static int RC_SIGN_IN = 94;

    // Extras / arguments

    public static final String EXTRA_SIGN_IN_ACCOUNT = "EXTRA_SIGN_IN_ACCOUNT";

    GoogleApiClient mGoogleApiClient;

    // Login status

    public static final int LOGIN_GOOGLE_OK = 11;

    public static final int LOGIN_GOOGLE_FAILED = 10;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate");

        // Init content
        setContentView(R.layout.activity_login);

        // Set onClick listener for the button
        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        if(signInButton != null)
            signInButton.setOnClickListener(this);

        // Configure sign-in to request the user's ID, email address, and basic
        //  profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions googleSignInOptions =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                        .requestProfile()
                    .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        //  options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .addApi(LocationServices.API)
                .build();

        // Try auto login
        // Save state in shared preferences
        SharedPreferences sharedPreferences =
                getSharedPreferences(Config.Prefs.MAIN_PREFERENCES, Activity.MODE_PRIVATE);
        String loggedIn = sharedPreferences.getString(Config.Prefs.LOGGED_IN, "");

        if(loggedIn.equals(Config.Prefs.LOGGED_IN_GOOGLE)) {
            signIn();
        } else {
            Log.d(TAG, "Need to sign in manually");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    // Perform sign in
    private void signIn() {
        Log.d(TAG, "Sign in");
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    // Handles the sign in result
    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "Handle sign in result");

        String loginStatus = "";
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();

            // Set login status
            loginStatus = Config.Prefs.LOGGED_IN_GOOGLE;

            Log.d(TAG, "Sign in successful");
            Intent data = new Intent();
            data.putExtra(EXTRA_SIGN_IN_ACCOUNT, acct);
            setResult(LOGIN_GOOGLE_OK, data);
            finish();
        } else {
            Log.d(TAG, "Sign in failed");
            setResult(LOGIN_GOOGLE_FAILED);
            finish();
        }

        // Save state in shared preferences
        SharedPreferences sharedPreferences =
                getSharedPreferences(Config.Prefs.MAIN_PREFERENCES, Activity.MODE_PRIVATE);

        sharedPreferences.edit()
                .putString(Config.Prefs.LOGGED_IN, loginStatus)
                .apply();

    }
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.sign_in_button) {
            signIn();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    /*
        ACTIVITY LIFETIME
     */

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
