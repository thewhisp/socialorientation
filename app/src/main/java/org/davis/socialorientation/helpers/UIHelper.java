package org.davis.socialorientation.helpers;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.TypedValue;

import org.davis.socialorientation.R;

/**
 * Created by davis on 06/04/16.
 */
public final class UIHelper {
    
    // Gets accent color
    public static int getAccentColor(final Context context) {
        TypedValue typedValue = new TypedValue();

        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[]
                {R.attr.colorAccent});
        int color = a.getColor(0, 0);

        a.recycle();

        return color;
    }
}
