package org.davis.socialorientation.adapters.lists;

/*
 * Created by Dāvis Mālnieks on 14/04/2015
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import org.davis.socialorientation.R;
import org.davis.socialorientation.config.Config;
import org.davis.socialorientation.containers.Person;
import org.davis.socialorientation.interfaces.OnRecyclerViewLazyItemClick;
import org.davis.socialorientation.tasks.BitmapWorkerTask;
import org.davis.socialorientation.utils.TimeUtils;
import org.davis.socialorientation.utils.cache.bitmap.BitmapUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class PersonAdapter extends
        RecyclerView.Adapter<PersonAdapter.ViewHolder> {

    List<Person> mItems;

    OnRecyclerViewLazyItemClick mLazyItemClickListener;

    Bitmap mPlaceholder;

    public PersonAdapter(List<Person> source) {
        this.mItems = source;
    }

    public PersonAdapter(final Context context) {
        mPlaceholder = BitmapUtils.decodeSampledBitmapFromResource(R.drawable.circular_placeholder,
                context.getResources(), 90, 90);
    }

    public void setOnRecyclerViewLazyItemClick(OnRecyclerViewLazyItemClick listener) {
        this.mLazyItemClickListener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView circleImageView;

        TextView twName;

        ImageButton ibMenu;

        TextView twDistance;

        TextView twDate;

        View rootView;

        ViewHolder(View v) {
            super(v);

            rootView = v;

            circleImageView = (CircleImageView) v.findViewById(R.id.profile_image);

            twDistance = (TextView) v.findViewById(R.id.tw_last);

            twDate = (TextView) v.findViewById(R.id.tw_date);

            ibMenu = (ImageButton) v.findViewById(R.id.person_list_popup);

            twName = (TextView) v.findViewById(R.id.tw_name);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row;

        row = LayoutInflater.from(parent.getContext()).inflate(R.layout.person_list_item, parent, false);

        return new ViewHolder(row);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        // Image
        int mImgSize = Config.Ui.Image.SIZE_PROFILE_LIST;
        int[] size = {mImgSize, mImgSize};

        // Load bitmap asynchronously
        BitmapWorkerTask.loadBitmap(mItems.get(position).getProfileImagePath(),
                holder.circleImageView, mPlaceholder, size);

        // Name
        holder.twName.setText(mItems.get(position).getName());

        // Distance
        holder.twDistance.setText(new DecimalFormat("##.##").format(
                mItems.get(position).getDistance() * 1000) + " meters");

        // Date
        Pair<Long, TimeUnit> lastSeen =
                TimeUtils.getLastActivity(TimeUtils.parseLastActivityDate(mItems.get(position).getDate()));
        String strLastSeen;

        if(lastSeen.second == TimeUnit.SECONDS) {
            strLastSeen = "seconds";
        } else if(lastSeen.second == TimeUnit.MINUTES) {
            strLastSeen = "minutes";
        } else if(lastSeen.second == TimeUnit.HOURS) {
            strLastSeen = "hours";
        } else {
            strLastSeen = "days";
        }
        strLastSeen = strLastSeen.concat(" ago");

        holder.twDate.setText(lastSeen.first + " " + strLastSeen);

        // Click listener
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mLazyItemClickListener != null)
                    mLazyItemClickListener.onItemClick(view);
            }
        });

        // Long click listener
        holder.rootView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(mLazyItemClickListener != null) {
                    mLazyItemClickListener.onItemLongClick(view);
                    return true;
                }
                return false;
            }
        });


        holder.ibMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mLazyItemClickListener != null)
                    mLazyItemClickListener.onIndividualItemClick(view);
            }
        });
    }

    /**
     * Updates the data of this adapter
     * @param newData New data
     */
    public void updateData(List<Person> newData) {
        if(newData == null) return;

        mItems = null;
        mItems = new ArrayList<>(newData);
        notifyDataSetChanged();
    }

    /**
     * Gets specific data item
     * @param position Position in the list
     * @return Playlist object
     */
    public Person getItem(int position) {
        return mItems.get(position);
    }

    /**
     * Removes specific item from data set
     * @param position Position of the item in the list
     */
    public void removeItem(int position) {
        this.mItems.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        if(mItems == null) return 0;
        return mItems.size();
    }
}
