package org.davis.socialorientation.database.preferences;

/*
 * Created by davis on 16/05/16.
 */

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public final class SharedPreferencesHelper {

    public static final String DEFAULT_SHARED_PREFS = "default";

    private static String mSharedPrefsStorage = DEFAULT_SHARED_PREFS;

    /* Saves something to shared preferences (boolean) */
    public static void saveBooleanPreference(final Context context,
                                             String key, boolean value) {
        SharedPreferences.Editor editor = getEditor(context);
        if(editor == null) return;
        editor.putBoolean(key, value);
        editor.apply();
    }

    /* Saves something to shared preferences (boolean) */
    public static void saveIntPreference(final Context context,
                                         String key, int value) {
        SharedPreferences.Editor editor = getEditor(context);
        if(editor == null) return;
        editor.putInt(key, value);
        editor.apply();
    }

    /* Saves something to shared preferences (String) */
    public static void saveStringPreference(final Context context,
                                            String key, String value) {
        SharedPreferences.Editor editor = getEditor(context);
        if(editor == null) return;
        editor.putString(key, value);
        editor.apply();
    }

    /* Gets boolean preference */
    public static boolean getBooleanPreference(final Context context, String key, boolean def) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        if(sharedPreferences == null) return false;
        return sharedPreferences.getBoolean(key, def);
    }

    /* Gets integer preference */
    public static int getIntPreference(final Context context, String key, int def) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        if(sharedPreferences == null) return 0;
        return sharedPreferences.getInt(key, def);
    }

    /* Gets String preference */
    public static String getStringPreference(final Context context, String key, String def) {
        SharedPreferences sharedPreferences = getSharedPreferences(context);
        if(sharedPreferences == null) return null;
        return sharedPreferences.getString(key, def);
    }


    /*
       UTILITIES
     */

    /* Sets the shared pref. name */
    public static void setSharedPrefStorage(String prefStorage) {
        mSharedPrefsStorage = prefStorage;
    }

    /* Gets the editor */
    private static SharedPreferences.Editor getEditor(final Context context) {
        if(context == null) return null;
        return context.getSharedPreferences(mSharedPrefsStorage,
                Activity.MODE_PRIVATE).edit();
    }

    /* Gets SharedPreferences */
    private static SharedPreferences getSharedPreferences(final Context context) {
        if(context == null) return null;
        return context.getSharedPreferences(mSharedPrefsStorage,
                Activity.MODE_PRIVATE);
    }

    /*
       KEYS
     */

    public final class Keys {
        public static final String USER_STATUS_MESSAGE = "user_status_message";

        public static final String USER_STATUS = "user_status_int"; // TODO

        public static final String REG_ID = "gcm_reg_id";
    }

}
