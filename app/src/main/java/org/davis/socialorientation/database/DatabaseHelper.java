package org.davis.socialorientation.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/*
 * Created by davis on 16/05/16.
 */

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 *
 *      Extend this class and add methods to get/save data
 */
public class DatabaseHelper {

    public static final String TAG = "DatabaseHelper";

    private Context mContext;

    private DatabaseConnectionHelper mDatabaseHelper;

    private SQLiteDatabase mDB;

    public DatabaseHelper(final Context context) {
        this.mContext = context;
    }

    // Opens connection to database
    public boolean open() {
        try {
            mDatabaseHelper = new DatabaseConnectionHelper(mContext);
            mDB = mDatabaseHelper.getWritableDatabase();
        } catch (Exception e) {
            Log.e(TAG, "Failed to open database: " + e.getLocalizedMessage());

            mDatabaseHelper = null;
            mDB = null;
            return false;
        }

        return true;
    }

    // Closes the database. NOTICE: MUST BE always called when done working with DB
    public void close() {
        if(!isInitialized()) {
            Log.w(TAG, "Attempt to close already closed (or not initialized) database");
            return;
        }

        mDB.close();
        mDatabaseHelper.close();
    }

    // Checks if the database is initialized
    public boolean isInitialized() {
        return mDB != null && mDatabaseHelper != null && mDB.isOpen();
    }
}
