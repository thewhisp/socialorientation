package org.davis.socialorientation.database;

/*
 * Created by Dāvis Mālnieks on 30/06/2015
 */

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class DatabaseConnectionHelper extends SQLiteOpenHelper {

    public static final String TAG = "DbConnectionHelper";

    public static final boolean DEBUG = false;

    // MAIN

    private static final String DB_NAME = "user.db";

    private static final int DB_VERSION = 1;

    // TABLES

    public static final String DB_PERSON_TABLE = "user_info";

    /* DB_PERSON_TABLE */ // TODO: Locally cached person data (mostly for chat history)

    public static final String _ID = "_id";

    public static final String USER_ID = "user_id";

    public static final String USER_STATUS = "user_status";


    /* CREATE TABLE QUERIES */

    public static final String CREATE_SHOW_TABLE_SQL = "CREATE TABLE " + DB_PERSON_TABLE + " (" +
            _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            USER_ID + " VARCHAR(255) NOT NULL UNIQUE, " +
            USER_STATUS + " VARCHAR(255) DEFAULT NULL);";


    public DatabaseConnectionHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            sqLiteDatabase.execSQL(CREATE_SHOW_TABLE_SQL);

            if(DEBUG)
                Log.d(TAG, "Database created!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        if (DEBUG)
            Log.d(TAG, "Upgrade database");
    }
}
