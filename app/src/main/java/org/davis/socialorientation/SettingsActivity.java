package org.davis.socialorientation;

/*
 * Created by Dāvis Mālnieks on 07/07/2015
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import org.davis.socialorientation.fragments.FragmentSettings;
import org.davis.socialorientation.utils.UiUtils;

/**
 * @author Dāvis Mālnieks <malnieks.davis@gmail.com>
 */
public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set layout
        setContentView(R.layout.activity_settings);

        // Setup toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        if(toolbar != null) {
            toolbar.setTitle("Settings");
            toolbar.setBackgroundColor(UiUtils.getAccentColor(this));

            // Set the toolbar as action bar
            setSupportActionBar(toolbar);

            final ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setElevation(5);
            }
        }

        // Display the fragment as the main content.
        if(savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container_settings, new FragmentSettings())
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        this.finish();
        backFromSettings();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            backFromSettings();
        }

        return false;
    }

    /**
     * Navigates back to main activity recreating it
     */
    public void backFromSettings() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}

